package org.fit.task3.repository;

import org.fit.task3.entity.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(value = "SELECT * FROM nodes " +
        "WHERE earth_distance(ll_to_earth(?1, ?2), ll_to_earth(nodes.latitude, nodes.longitude)) < ?3 " +
        "ORDER BY earth_distance(ll_to_earth(?1, ?2), ll_to_earth(nodes.latitude, nodes.longitude))",
        nativeQuery = true)
    List<Node> getTheNearestNodes(double latitude, double longitude, double radius);
}
