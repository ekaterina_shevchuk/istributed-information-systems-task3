package org.fit.task3.service;

import lombok.RequiredArgsConstructor;
import org.fit.task3.entity.Node;
import org.fit.task3.repository.NodeRepository;
import org.fit.task3.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NodeService {
    @Autowired
    private final NodeRepository nodeRepository;
    @Autowired
    private final TagRepository tagRepository;

    public Node save(Node node) {
        tagRepository.saveAll(node.getTags());
        return nodeRepository.save(node);
    }

    public Node getNode(Long id) {
        Node node = nodeRepository.findById(id).orElseThrow();
        node.setTags(tagRepository.findAllByNode(node));
        return node;
    }

    public Node update(Long id, Node node) {
        Node nodeFromDb = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        node.setId(nodeFromDb.getId());
        return nodeRepository.save(node);
    }

    public void delete(long id) {
        Node node = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        nodeRepository.delete(node);
    }

    public List<Node> search(Double latitude, Double longitude, Double radius) {
        return nodeRepository.getTheNearestNodes(latitude, longitude, radius);
    }
}

