package org.fit.task3.controller;

import lombok.RequiredArgsConstructor;
import org.fit.task3.entity.Node;
import org.fit.task3.service.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/node")
@RequiredArgsConstructor
public class NodeController {

    @Autowired
    private final NodeService service;

    @PostMapping
    Node saveNode(@RequestBody Node node) {
        return service.save(node);
    }

    @GetMapping("/{id}")
    Optional<Node> getNode(@PathVariable("id") Long id) {
        return Optional.of(service.getNode(id));
    }

    @PutMapping("/{id}")
    Node updateNode(@PathVariable("id") Long id,
                    @RequestBody Node node) {
        return service.update(id, node);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        service.delete(id);
    }

    @GetMapping("/search")
    public List<Node> search(
        @RequestParam("latitude") Double latitude,
        @RequestParam("longitude") Double longitude,
        @RequestParam("radius") Double radius
    ) {
        return service.search(latitude, longitude, radius);
    }
}
