package org.fit.task3.repository;

import org.fit.task3.entity.Node;
import org.fit.task3.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findAllByNode(Node node);
}
